//==== Natural Search ===================================================== //

IF      CONTAINS([Channels],"Natural") THEN "Organic Search"

//==== Search ===================================================== //

ELSEIF  CONTAINS([Channels],"Search")
        AND  (CONTAINS([Campaign Name],"(G)")
             OR      CONTAINS([Campaign Name],"(GK)")
             OR      CONTAINS([Campaign Name],"(GR)"))
THEN "Generic Search"

ELSEIF  CONTAINS([Channels],"Search")
        AND( CONTAINS([Campaign Name],"(CMP)")
             OR  CONTAINS([Campaign Name],"(CMP)")
             OR  CONTAINS([Campaign Name],"(C)"))
THEN "Competition"

ELSEIF  CONTAINS([Channels],"Search")
    AND     (CONTAINS([Campaign Name],"(B)")
            OR      CONTAINS([Campaign Name],"(BK)")
            OR      CONTAINS([Campaign Name],"(B)")
            OR      CONTAINS([Campaign Name],"(BK)")) 
THEN "Branded Search"

ELSEIF  CONTAINS([Sub-Channel],"Gmail") AND REGEXP_MATCH(lower([Campaign Name (group)]),"zoo|tado") THEN "Prospecting"
ELSEIF  CONTAINS([Sub-Channel],"Gmail") AND REGEXP_MATCH(lower([Campaign Name (group)]),"dal promo 1") THEN "Retargeting"


//==== CRM Strategies ===================================================== //

ELSEIF CONTAINS(lower([Placement Name]),"popin") AND CONTAINS([Channels],"CRM") THEN "Pop In"
ELSEIF CONTAINS(lower([Placement Name]),"upsell") AND CONTAINS([Channels],"CRM") THEN "Upsell"
ELSEIF CONTAINS(lower([Placement Name]),"winback") AND CONTAINS([Channels],"CRM") THEN "Winback"
ELSEIF CONTAINS(lower([Placement Name]),"Acquisi") AND CONTAINS([Channels],"CRM") THEN "Acquisition"

//==== Affiliation ===================================================== //

ELSEIF  CONTAINS([Channels],"Affiliation") 
        AND CONTAINS([Placement Name],"Email")
THEN "Emailing"

ELSEIF  CONTAINS([Channels],"Affiliation") 
        AND REGEXP_MATCH(LOWER([Placement Name]),"display|cpm")
THEN "Display"

//==== Branding ===================================================== //

ELSEIF  REGEXP_MATCH(LOWER([Placement Name]),"youtube|teads|video|awareness|branding") 
        OR  REGEXP_MATCH(LOWER([Site Name]),"teads|youtube")
THEN "Awareness"

//==== Prospecting ===================================================== //

ELSEIF  REGEXP_MATCH(LOWER([Placement Name]),"prospecting|traffic|pros")
THEN "Prospecting"

//==== Remarketing ===================================================== //

ELSEIF  REGEXP_MATCH(lower([Placement Name]),"re(targe|marke)ting|rmkt")
        OR REGEXP_MATCH([Placement Id],"238279458|237883424|144956832|151372432|144955276|151370950|147806664|147805950|217351209|229407426|222805862")
THEN "Retargeting"

//==== Branding ===================================================== //

ELSEIF  CONTAINS([Placement Name],"Youtube") 
        OR  REGEXP_MATCH(LOWER([Site Name]),"teads|youtube")
THEN "Branding" 

//==== Miscallenous ===================================================== //

ELSEIF [Channels]="Prijsvergelijkers" THEN "Prijsvergelijkers"
ELSEIF [Channels]="Direct" THEN "Web Direct"
ELSE "TBD"

END