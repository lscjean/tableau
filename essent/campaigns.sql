// ===DAL Promo #2================= //
IF      [Date] >= #2019-04-14#
        AND (CONTAINS(LOWER([Campaign Name (group)]),"dal")
            OR      CONTAINS(LOWER([adlabels]),"dal"))
THEN    "Dal Promo 2"

// ===DAL Promo #1================= //

ELSEIF  ([Date] >= #2019-01-10# AND [Date] < #2019-04-14#)
        AND (CONTAINS(LOWER([Campaign Name (group)]),"dal")
             OR      CONTAINS(LOWER([adlabels]),"dal"))
THEN    "Dal Promo 1"

// ===Zoo Promo================= //
ELSEIF CONTAINS([Channels],"Search") AND LOWER([adlabels]) = "zoo" 
        OR CONTAINS(LOWER([Campaign Name]),"zoo")
THEN "Zoo Promo"

ELSEIF CONTAINS([Channels],"Search") AND 
       REGEXP_MATCH([Campaign Id],"71700000018211933|71700000018211903|71700000018211912|71700000018211906|71700000018211909|71700000018220283|71700000018211915|71700000048990930|71700000018211936|71700000018211939|71700000018203124|71700000018211930|71700000018211942|71700000018211924|71700000018211927|71700000043787502|71700000043787499|71700000043787496|71700000043787487")
THEN "Always On"

ELSE [Campaign Name (group)]

END