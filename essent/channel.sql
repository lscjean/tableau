IF  CONTAINS([Site Name],"Awin")
    OR      CONTAINS([Site Name],"Zanox")
    OR      CONTAINS([Site Name],"kwanko")
THEN  "Affiliation"

ELSEIF  REGEXP_MATCH([Site Name],"Mediascale")
        OR      CONTAINS([Site Name],"outlook")
        OR      CONTAINS(LOWER([Site Name]),"youtube")
        OR      CONTAINS(LOWER([Site Name]),"ligatus")
        OR      CONTAINS([Site Name],"Cadreon")
        OR      CONTAINS([Site Name],"Mediahuis")
        OR      CONTAINS([Site Name],"Google Display Network")
        OR      CONTAINS([Site Name],"DePersgroep")
        OR      CONTAINS(LOWER([Site Name]),"just premium")
        OR      CONTAINS(LOWER([Site Name]),"rossel")
THEN  "Display"


ELSEIF CONTAINS([Site Name],"Natural") THEN "Natural Search"

ELSEIF  CONTAINS(LOWER([Site Name]),"adwords")
        OR      CONTAINS(LOWER([Placement Name]),"gmail")
        OR      CONTAINS([Site Name],"Bing")
THEN  "Search"

ELSEIF  CONTAINS([Site Name],"Essent")
THEN  "CRM"

ELSEIF  CONTAINS(LOWER([Site Name]),"facebook")
THEN  "Social"

END