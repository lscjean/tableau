IF 
            REGEXP_MATCH(LOWER([Placement Name]),"(fr$)|(( |_)fr( |_))|(befr)")
    OR      REGEXP_MATCH(LOWER([Campaign Name]),"(fr$)|(( |_)fr( |_))|(befr)")
//  OR      REGEXP_MATCH([Campaign Id],"") /** DUMP **/
    THEN "Belgium French"

ELSEIF 
            REGEXP_MATCH(LOWER([Placement Name]),"(nl$)|(( |_)nl( |_))|(benl)")
    OR      REGEXP_MATCH(LOWER([Campaign Name]),"(nl$)|(( |_)nl( |_))|(benl)")
//  OR      REGEXP_MATCH([Campaign Id],"") /** DUMP **/
    THEN "Belgium Dutch"

ELSEIF 
    LOWER([Placement Name]) = "natural search" 
    OR  LOWER([Campaign Name (group)]) = "natural search" 
    THEN "Organic Search"

ELSE "TBD"

END