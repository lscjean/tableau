//== Mediascale ====================================//

IF 
([Site Name]="Mediascale BE"
OR [Site Name]="Mediascale" 
OR [Site Name]="Teads"
OR [Site Name]="YouTube")
AND
        (CONTAINS(LOWER([Placement Name]),"video")
    OR  CONTAINS(LOWER([Placement Name]),"branding"))
OR  CONTAINS(LOWER([Site Name]),"sublimeskinz")
OR  CONTAINS(LOWER([Site Name]),"just premium")
THEN "Mediascale Branding"

ELSEIF 
([Site Name]="Mediascale BE" OR [Site Name]="Mediascale" )
AND 
        CONTAINS(LOWER([Placement Name]),"prospecting")
THEN "Mediascale Prospecting"

ELSEIF 
([Site Name]="Mediascale BE" OR [Site Name]="Mediascale" )
AND(
        CONTAINS(LOWER([Placement Name]),"retargeting")
    OR  CONTAINS(LOWER([Placement Name]),"rmkt"))
THEN "Mediascale Retargeting"


//== Direct ====================================//

ELSEIF CONTAINS(LOWER([Site Name]),"skynet") THEN "Direct Skynet"
ELSEIF CONTAINS(LOWER([Site Name]),"pebble") THEN "DirectPebble"
ELSEIF CONTAINS(LOWER([Site Name]),"ad.nl") THEN "Direct Ad.nl"
ELSEIF CONTAINS(LOWER([Site Name]),"blogmij") THEN "Direct Blogmij"
ELSEIF CONTAINS(LOWER([Site Name]),"brandvoice") THEN "Direct brandvoice"
ELSEIF LOWER([Site Name])="mediahuis.be" THEN "Direct Mediahuis"
ELSEIF LOWER([Site Name])="jani tv" THEN "Direct JaniTV"
ELSEIF LOWER([Site Name])="hln be" THEN "Direct HLN"
ELSEIF LOWER([Site Name])="edenred" THEN "Direct Edenred"
ELSEIF LOWER([Site Name])="7sur7 be" THEN "Direct 7sur7"
ELSEIF LOWER([Site Name])="medical marketing & media" THEN "Direct MMM"

ELSEIF CONTAINS(LOWER([Placement Name]),"insertionorder")
    THEN REPLACE("Direct "+REPLACE([Site Name],' ',''),".","")


//== Facebook ====================================//

ELSEIF 
([Site Name]="Facebook" OR [Site Name]="Instagram")
AND 
        CONTAINS(LOWER([Placement Name]),"prospecting")
THEN "Facebook Prospecting"

ELSEIF 
([Site Name]="Facebook" OR [Site Name]="Instagram")
AND 
        CONTAINS(LOWER([Placement Name]),"retargeting")
THEN "Facebook Retargeting"

ELSEIF 
([Site Name]="Facebook" OR [Site Name]="Instagram")
AND (
        CONTAINS(LOWER([Placement Name]),"branding")
    OR  CONTAINS(LOWER([Placement Name]),"branded"))
THEN "Facebook Branding"



//== SnapChat ====================================//

ELSEIF 
[Site Name]="Snapchat"
AND 
        CONTAINS(LOWER([Placement Name]),"branding")
THEN "Snapchat Branding"


//== Search ====================================//

ELSEIF 
[Site Name]="Adwords"
AND 
        CONTAINS(LOWER([Placement Name]),"branded")
THEN "Google Brand"

ELSEIF 
[Site Name]="Adwords"
AND 
        CONTAINS(LOWER([Placement Name]),"generic")
THEN "Google Generic"

ELSEIF 
[Site Name]="Adwords"
AND 
        CONTAINS(LOWER([Placement Name]),"competition")
THEN "Google Competitor"

ELSEIF 
[Site Name]="Adwords"
AND 
        CONTAINS(LOWER([Placement Name]),"prospecting")
THEN "Google Generic"

ELSEIF 
[Site Name]="Bing"
AND 
        CONTAINS(LOWER([Placement Name]),"generic")
THEN "Bing Generic"

ELSEIF 
[Site Name]="Bing"
AND 
        CONTAINS(LOWER([Placement Name]),"branded")
THEN "Bing Brand"

ELSEIF 
    CONTAINS(LOWER([Site Name]),"dart search")      
OR  CONTAINS(LOWER([Site Name]),"organic")
THEN "Natural Search"



//== Affiliation ============================//

ELSEIF [Site Name] = "Trade Doubler" THEN "Tradedoubler"
ELSEIF CONTAINS(LOWER([Site Name]),"tradedoubler") THEN "Tradedoubler"
ELSEIF CONTAINS(LOWER([Site Name]),"tradetracker") THEN "Tradetracker"
ELSEIF CONTAINS(LOWER([Site Name]),"daisycon") THEN "DaisyCon"
ELSEIF CONTAINS(LOWER([Site Name]),"awin") OR CONTAINS(LOWER([Site Name]),"zanox") THEN "Awin|Zanox"
ELSEIF CONTAINS(LOWER([Site Name]),"affiliate4you") THEN "Affiliate4you"
ELSEIF [Site Name]="Adfactor NL" THEN "Adfactor"
ELSEIF [Site Name]="Kwanko" THEN "Kwanko"
ELSEIF [Site Name]="FamilyBlendNL" THEN "Family Blend"



//== eCRM ============================//

ELSEIF 
([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers") 
    AND (CONTAINS(LOWER([Placement Name]),"ezine"))
THEN "eZine"

ELSEIF 
([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers") 
    AND CONTAINS(LOWER([Placement Name]),"promo")
THEN "Promo"

ELSEIF 
([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers") 
    AND CONTAINS(LOWER([Placement Name]),"abandon")
THEN "Abandon"

ELSEIF 
([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers") 
    AND CONTAINS(LOWER([Placement Name]),"eshop")
THEN "eShop"

ELSEIF 
([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers") 
    AND CONTAINS(LOWER([Placement Name]),"foodbox")
THEN "foodBox"

ELSEIF 
([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers") 
    AND CONTAINS(LOWER([Placement Name]),"inviteafriend")
THEN "Invite a Friend"

ELSEIF 
CONTAINS(LOWER([Placement Name]),"sms")
THEN "SMS"

ELSEIF 
([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers" )
    AND CONTAINS(LOWER([Placement Name]),"email")
THEN "other Emails"

ELSEIF 
(([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers")
    AND CONTAINS(LOWER([Placement Name]),"crm"))
OR ([Site Name]="WeightWatchers" OR [Site Name]="Weight Watchers")
THEN "Other CRM"

END