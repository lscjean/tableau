//=== Exceptions =======================================//
IF 
        CONTAINS(LOWER([Placement Name]),"video")
    OR  CONTAINS(LOWER([Placement Name]),"branding")

THEN "Branding"

ELSEIF  
[Site Name]="Mediascale BE"
AND
        CONTAINS(LOWER([Placement Name]),"prospecting")
    OR  CONTAINS(LOWER([Placement Name]),"retargeting")

THEN "Performance"


//=== Branding ==========================================//
ELSEIF  
        CONTAINS(LOWER([Campaign Name]),"winterseason")
    OR  CONTAINS(LOWER([Campaign Name]),"summerofimpact")

THEN "Branding"

//=== Performance =======================================//

ELSE "Performance"

END