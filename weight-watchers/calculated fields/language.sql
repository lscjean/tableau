/// AdServer data
LOWER(
IF      CONTAINS(LOWER([Advertiser (group)]),"befr")
OR      CONTAINS(LOWER([Campaign Name]),"befr")
OR      CONTAINS(LOWER([Placement Name]),"befr")
OR      CONTAINS(LOWER([Site Name]),"befr")
OR      CONTAINS(LOWER([Placement Name]),"befr")
THEN    "BEFR"
  
ELSEIF  CONTAINS(LOWER([Advertiser (group)]),"benl")
OR      CONTAINS(LOWER([Campaign Name]),"benl")
OR      CONTAINS(LOWER([Placement Name]),"benl")
OR      CONTAINS(LOWER([Site Name]),"benl")
OR      CONTAINS(LOWER([Placement Name]),"benl")
THEN    "BENL"
  
ELSEIF  CONTAINS(LOWER([Advertiser (group)]),"nlnl")
OR      CONTAINS(LOWER([Campaign Name]),"nlnl")
OR      CONTAINS(LOWER([Placement Name]),"nlnl")
OR      CONTAINS(LOWER([Site Name]),"nlnl")
OR      CONTAINS(LOWER([Placement Name]),"nlnl")
THEN    "NLNL"
END
)