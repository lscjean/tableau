with
    base as (
     select * from weightwatchers.dashboard_daily_nl
     union all
     (
         select * from weightwatchers.dashboard_daily_be
     )
    ),
    base_2 as (
        select * ,
    case
        when
            lower(advertiser_name_norm) like '%befr%'  or
            lower(campaign_name) like '%befr%'  or
            lower(placement_name) like '%befr%'  or
            lower(site_name) like '%befr%'
        then 'BEFR'
        when
            lower(advertiser_name_norm) like '%benl%'  or
            lower(campaign_name) like '%benl%'  or
            lower(placement_name) like '%benl%'  or
            lower(site_name) like '%benl%'
        then 'BENL'
        when
            lower(advertiser_name_norm) like '%nl%'  or
            lower(campaign_name) like '%nlnl%'  or
            lower(placement_name) like '%nlnl%'  or
            lower(site_name) like '%nlnl%'
        then 'NLNL'
        else '?'
    end as language,



    case
        when
            lower(placement_name) like '%video%'  or
            lower(placement_name) like '%branding%'
        then 'Branding'
        when
            site_name in ('Mediascale BE','Mediascale') and
                (
                lower(placement_name) like '%prospecting%' or
                lower(placement_name) like '%retargeting%'
                )
        then 'Performance'
        when
            lower(campaign_name) like '%winterseason%'  or
            lower(campaign_name) like '%summerofimpact%'
        then 'Branding'
        when
            campaign_name in ('Organic') then 'Organic'
        else 'Performance'

    end as action,
    case
        when
            (
            site_name in ('Mediascale BE','Mediascale')  or
            site_name = 'Teads' or
            site_name = 'YouTube'
            )
        and
            (
            lower(placement_name) like '%video%'  or
            lower(placement_name) like '%branding%'
            )
        or
            lower(site_name) like '%sublimeskinz%'
        or
            site_name   = 'Just Premium'
        then 'Mediascale Branding'

         when
            (
            site_name in ('Mediascale BE','Mediascale') and
            lower(placement_name) like '%prospecting%')
        then 'Mediascale Retargeting'


        when
            site_name in ('Mediascale BE','Mediascale')
        and
            (
            lower(placement_name) like '%retargeting%'  or
            lower(placement_name) like '%rmkt%'
            )
        then 'Mediascale Retargeting'
        when lower(site_name) like '%skynet%' then 'Direct Skynet'
        when lower(site_name) like '%pebble%' then 'DirectPebble'
        when lower(site_name) like '%ad.nl%' then 'Direct Ad.nl'
        when lower(site_name) like '%blogmij%' then 'Direct Blogmij'
        when lower(site_name) like '%brandvoice%' then 'Direct brandvoice'
        when lower(site_name) like '%mediahuis.be%' then 'Direct Mediahuis'
        when lower(site_name) like '%jani tv%' then 'Direct JaniTV'
        when lower(site_name) like '%hln be%' then 'Direct HLN'
        when lower(site_name) like '%edenred%' then 'Direct Edenred'
        when lower(site_name) like '%7sur7 be%' then 'Direct 7sur7'
        when lower(site_name) like '%medical marketing & media%' then 'Direct MMM'
        when lower(site_name) like '%insertionorder%' then 'Direct '|| replace(replace(lower(site_name),' ',''),'.','')
        when
            (lower(site_name) like '%facebook%' or lower(site_name) like '%instagram%') and
            lower(placement_name) like '%prospecting%'
        then 'Facebook Prospecting'
        when
            (lower(site_name) like '%facebook%' or lower(site_name) like '%instagram%') and
            (lower(placement_name) like '%retargeting%')
        then 'Facebook Retargeting'
        when
            (lower(site_name) like '%facebook%' or lower(site_name) like '%instagram%') and
            (lower(placement_name) like '%branding%' or lower(placement_name) like '%branded%' )
        then 'Facebook Branding'
        when
            (lower(site_name) like '%snapchat%') and
            (lower(placement_name) like '%branding%')
        then 'Snapchat Branding'


         when
            (site_name = 'Adwords') and
            (lower(placement_name) like '%branded%')
        then 'Google Brand'
         when
            (site_name = 'Adwords') and
            (lower(placement_name) like '%generic%')
        then 'Google Generic'
         when
            (site_name = 'Adwords') and
            (lower(placement_name) like '%competition%')
        then 'Google Competitor'


        when
            (site_name = 'Adwords') and
            (lower(placement_name) like '%generic%')
        then 'Google Generic'
        when
            (site_name = 'Bing' ) and
            (lower(placement_name) like '%generic%')
        then 'Bing Generic'
        when
            (lower(site_name) like '%dart search%') or
            (lower(placement_name) like '%organic%')
        then 'Natural Search'


        when site_name in ( 'Trade Doubler','TradeDoubler BE') then 'Tradedoubler'
        when  site_name = 'TradeTracker DE' THEN 'TradeTracker DE'
        when  site_name = 'Daisycon NL' THEN 'DaisyCon'
        when  site_name  in ('Awin','Zanox')  then  'Awin|Zanox'
        when  site_name = 'affiliate4you' THEN 'Affiliate4you'
        when  site_name = 'Adfactor NL' THEN 'Adfactor'
        when  site_name = 'Kwanko' THEN 'Kwanko'
        when  site_name =  'FamilyBlendNL' THEN 'Family Blend'

        when
            site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%ezine%'
        THEN 'eZine'
        when
            site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%promo%'
        THEN 'promo'
        when
            site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%abandon%'
        THEN 'Abandon'
        when
            site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%eshop%'
        THEN 'eShop'
        when
           site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%eshop%'
        THEN 'foodbox'
        when
            site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%Invite a Friend%'
        THEN 'Invite a Friend'
        when
            site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%Invite a Friend%'
        THEN 'Invite a Friend'
        when
            lower(placement_name) like '%sms%'
        THEN 'SMS'
        when
            site_name in ('Weight Watchers','WeightWatchers')  and
            lower(placement_name) like '%email%'
        THEN 'other Emails'

         when
            ( site_name in ('Weight Watchers','WeightWatchers')  and
             (lower(placement_name) like '%crm%') )
         or (lower(site_name) in ('Weight Watchers','WeightWatchers')

            )
        THEN 'Other CRM'
        when
            campaign_name in ('Organic') then 'Organic'
    else '?'
    end as source,

    case
        when site_name in (
                            'Adfactor NL',
                            'Affiliate4you',
                            'Awin',
                            'Awin | Zanox',
                            'DaisyCon',
                            'Daisycon NL',
                            'FamilyBlendNL',
                            'Kwanko',
                            'Trade Doubler',
                            'TradeDoubler',
                            'TradeDoubler BE',
                            'TradeTracker',
                            'TradeTracker DE'
                            ) then 'Affiliation Total'
        when site_name in (
                        'Weight Watchers',
                        'WeightWatchers',
                        'sms')then 'eCRM Total'
        when site_name in (
                        '7sur7 BE',
                        'ad.nl',
                        'Blogmij',
                        'Brandvoice NL',
                        'Edenred',
                        'femmefab.nl',
                        'hi-media.be',
                        'HLN BE',
                        'Jani TV',
                        'm4n.nl',
                        'mamaenzo.nl',
                        'Marktplaats NL -',
                        'Me-to-we.nl',
                        'mediahuis.be',
                        'medialaan.net',
                        'Medical Marketing & Media',
                        'onlineactivity.nl',
                        'Pebble',
                        'pebble.be',
                        'persgroepadvertising.be',
                        'sanoma.be',
                        'Skynet.be',
                        'Skynet',
                        'Vivio',
                        'Zuiver Media',
                        'Zwangerschapspagina'
                                ) then 'IO Total'
        when site_name in (
                        'Just Premium',
                        'Mediascale',
                        'Mediascale BE',
                        'Sublimeskinz',
                        'Teads',
                        'YouTube'
                        ) then 'Media Total'
        when site_name in (
                        'Adwords',
                        'Bing',
                        'Dart Search'
        ) then 'Search Total'
        when site_name  in
                        ('Facebook',
                        'Instagram',
                        'Snapchat'
                        )then 'Social Total'
    end as channels

    from base
    )
    select distinct
        *,
        language,
        action,
        source,
        case when site_name  in (
                            'Adfactor NL',
                            'Affiliate4you',
                            'Awin',
                            'Awin | Zanox',
                            'DaisyCon',
                            'Daisycon NL',
                            'FamilyBlendNL',
                            'Kwanko',
                            'Trade Doubler',
                            'TradeDoubler',
                            'TradeDoubler BE',
                            'TradeTracker',
                            'TradeTracker DE',
                            'Weight Watchers',
                            'WeightWatchers',
                            'sms'
                            ) then 'Affiliation Total'
            else split_part(source,' ',2) end as strategy,
        channels

    from base_2