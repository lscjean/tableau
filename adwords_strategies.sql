/*==== Search ===================================================== 
    campaign_name, ad_group_name are coming from Adwords DB
    placement are coming from DCM
    channels is define by the data source, here : 'adwords'
*/


IF  CONTAINS([Channels],"Search") 
    AND (
            CONTAINS([campaign_name],"Gmail") 
            OR CONTAINS([placement],"Gmail")
        )

THEN "Gmail Ad"

ELSE IF  CONTAINS([Channels],"Search")
        AND (
                CONTAINS([placement],"Generic")
                OR CONTAINS([campaign_name],"(G)")
                OR CONTAINS([campaign_name],"(GK)")
                OR CONTAINS([campaign_name],"(GR)")
            )
THEN "Generic Search"

ELSE IF  CONTAINS([Channels],"Search")
        AND (
                CONTAINS([campaign_name],"Competitor")
                OR CONTAINS([campaign_name],"(CMP)")
                OR CONTAINS([campaign_name],"(C)")
            )   

THEN "Competition"

ELSE IF  CONTAINS([Channels],"Search")
        AND (
                CONTAINS([placement],"Branded")
                OR CONTAINS([campaign_name],"(B)")
                OR CONTAINS([campaign_name],"(BK)")
                OR CONTAINS([placement],"DagNacht")
                OR CONTAINS([placement],"Essent")
            )
        
THEN "Branded Search"

ELSE IF  CONTAINS([ad_group_name],"Interest") 
THEN "Interests"

ELSE IF  CONTAINS([ad_group_name],"Smart Display") 
THEN "Smart Display"